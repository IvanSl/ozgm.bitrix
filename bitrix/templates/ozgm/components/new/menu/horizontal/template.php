<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>
<nav class="header-nav">

	<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
		<span class="sr-only">Toggle navigation</span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
	</button>
	<div id="navbar" class="ozgm-nav navbar-collapse collapse in">
		<ul class="">
<? foreach($arResult as $arItem):?>
		<li><a href="<?=htmlspecialcharsbx($arItem["LINK"])?>"><?=htmlspecialcharsbx($arItem["TEXT"])?><span>&nbsp;&rsaquo;&rsaquo;</span></a></li>
<?endforeach?>
		</ul>
	</div>
</nav>
<?endif?>