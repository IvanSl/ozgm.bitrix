<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>

                <nav class="ozgm-nav">                    
                  <ul>
                      <!--<li><a href="">Главная<span>&nbsp;&rsaquo;&rsaquo;</span></a></li>-->
                      <!--<li><a href="">О заводе<span>&nbsp;&rsaquo;&rsaquo;</span></a></li>-->
                      <!--<li><a href="">Продукция<span>&nbsp;&rsaquo;&rsaquo;</span></a></li>-->
                      <!--<li><a href="">Услуги<span>&nbsp;&rsaquo;&rsaquo;</span></a></li>-->
						<? foreach($arResult as $arItem):?>
								<li><a href="<?=htmlspecialcharsbx($arItem["LINK"])?>"><?=htmlspecialcharsbx($arItem["TEXT"])?><span>&nbsp;&rsaquo;&rsaquo;</span></a></li>
						<?endforeach?>
                      <!--<li><a href="contacts.php">Контакты<span>&nbsp;&rsaquo;&rsaquo;</span></a></li>-->
                  </ul>
                </nav>
<?endif?>