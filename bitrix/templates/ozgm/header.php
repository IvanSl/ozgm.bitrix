<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<?$APPLICATION->ShowHead()?>
<title><?$APPLICATION->ShowTitle()?></title>

</head>

<body>
<div id="panel"><?$APPLICATION->ShowPanel();?></div>

      <header id="header">
         <!--<img src="" alt="header-bg" id="header-bg">-->
         <div class="bg-dark-blue">
            <div class="container header-container">
                  <div class="row h-top">
<!--LOGO-->
<?$APPLICATION->IncludeFile(
$APPLICATION->GetTemplatePath("include_areas/logo_inc.php"),
Array(),
Array("MODE"=>"php")
);?>
<?$APPLICATION->IncludeComponent(
	"bitrix:menu",
	"horizontal",
	Array(
		"ROOT_MENU_TYPE" => "top", 
		"MAX_LEVEL" => "1", 
		// "CHILD_MENU_TYPE" => "left", 
		"USE_EXT" => "Y", 
		"MENU_CACHE_TYPE" => "A",
		"MENU_CACHE_TIME" => "3600",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"MENU_CACHE_GET_VARS" => Array()
	)
);?> 
                        <a href="" id="header-btn">презентация <span class="hidden-xs hidden-md">компании</span>&nbsp;<span class="header-btn-arrow">&rsaquo;&rsaquo;</span></a>
               <!--row h-top-->
                  </div>
               </div>
            </div>
         </div>
         </header>