<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
IncludeTemplateLangFile(__FILE__);
?> 


      <div class="bg-black">
         <div class="container" id="footer">
            <div class="row">
               <div class="col-xs-12">
                  <!--<img src="images/logo.png" alt="" class="logo">-->
                  <?$APPLICATION->IncludeFile(
                  $APPLICATION->GetTemplatePath("include_areas/logo_inc.php"),
                  Array(),
                  Array("MODE"=>"php")
                  );?>
                  <h1>АО Опытный завод &lsaquo;&lsaquo;Гидромонтаж&rsaquo;&rsaquo; 1992-2017</h1>
                  <div class="footer-address">
                     <p><a href="">143345, Московская область,<br>Наро-фоминский район, пос. Селятино,<br>территроия завода &lsaquo;&lsaquo;Гидромонтаж&rsaquo;&rsaquo;</a></p>
                  </div>
                  <div class="footer-contacts">
                  <!--how add phone value to link?-->
                     <a href="tel:+74957204964">Телефон: <span>
<?$APPLICATION->IncludeFile(
  $APPLICATION->GetTemplatePath("include_areas/footer_phone_1.php"),
  Array(),
  Array("MODE"=>"php")
);?>                 
                     </span></a><br>
                     <a href="fax:+7.495.720.4972">Тел./Факс: <span>+7 (495) 720-49-72</span></a><br>
                     <a href="mailto:info@ozgm.ru">Электронная почта: <span>info@ozgm.ru</span></a>
                  </div>
               </div>
               <div class="col-xs-12 footer-nav">
                <!--<nav class="ozgm-nav">                    
                  <ul>
                      <li><a href="">Главная<span>&nbsp;&rsaquo;&rsaquo;</span></a></li>
                      <li><a href="">О заводе<span>&nbsp;&rsaquo;&rsaquo;</span></a></li>
                      <li><a href="">Продукция<span>&nbsp;&rsaquo;&rsaquo;</span></a></li>
                      <li><a href="">Услуги<span>&nbsp;&rsaquo;&rsaquo;</span></a></li>
                      <li><a href="">Пресс-центр<span>&nbsp;&rsaquo;&rsaquo;</span></a></li>
                      <li><a href="contacts.php">Контакты<span>&nbsp;&rsaquo;&rsaquo;</span></a></li>
                  </ul>
                </nav>-->
                <?$APPLICATION->IncludeComponent(
	"bitrix:menu",
	"horizontal_footer",
	Array(
		"ROOT_MENU_TYPE" => "top", 
		"MAX_LEVEL" => "1", 
		// "CHILD_MENU_TYPE" => "left", 
		"USE_EXT" => "Y", 
		"MENU_CACHE_TYPE" => "A",
		"MENU_CACHE_TIME" => "3600",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"MENU_CACHE_GET_VARS" => Array()
	)
);?> 
                  <a href="http://vefound.com" class="vefound-info"><span class="hidden-md">Создание и поддержка сайта: </span>veFound.com</a>
               </div>
            </div>
         </div>
      </div>
<!--FOOTER-->

</body>
</html>