<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("article page2");
?>


<!--PAGE HEADING-->
  <div class="bg-gray-whisper">
    <div class="page-heading container">
      <h1>АО &lsaquo;&lsaquo;Опытный завод &lsaquo;&lsaquo;Гидромонтаж&rsaquo;&rsaquo;</h1>
      <p class="page-heading-sub-title-1">Предприятие промышленного холдинга</p>
      <p class="page-heading-sub-title-2">Производство опор вл и лэп</p>
    </div>
  </div>
<!--PAGE HEADING-->

<!--TODO: change name of class-->
<div class="bg-container">
  <img src="images/contacts-bg-1.png" alt="">  
<!--</div>-->
  <div class="container" >
<!--BREADCRUMBS-->
    <ul class="breadcrumbs bg-gray-whisper">
      <li class="breadcrumbs-home"><a href="">главная</a></li>
      <li class="breadcrumbs-active"><a href="">Контакты</a></li>
      <li class=""><a href="">Еще пункт</a></li>
    </ul>
<!--BREADCRUMBS-->
  </div>
  <!--NEWS PAGE-->
  <section class="page-content container">
    <div class="page-content-heading">услуги</div>
    <!--<div>-->
            <article class="ozgm-article row services-article">
                <p>В промышленный холдинг «Гидромонтаж» так же входят:</p>
                <ul>
                    <li><p><strong>ЗАО «Гидростальконструкция»</strong>  – компания по производству гидромеханического оборудования, строительных металлконструкций, многогранных опор ЛЭП, резервуаров различного назначения и крановых механизмов.</p></li>
                    <li><p><strong>ЗАО «Гофросталь»</strong>  – единственный в СНГ производитель металлических гофрированных конструкций всех типов и сечений.</p></li>
                    <li><p><strong>ООО «СевЗапРегионСтрой»</strong>  – компания, специализирующаяся на проектировании и строительстве объектов с масштабным применением продукции холдинга, обладающая всеми необходимым допусками и разрешениями.</p></li>
                    <li><p><strong>ООО ТК «Гидромонтаж»</strong>   – транспортная компания, оказывающая услуги по перевозке грузов любым видом транспорта.</p></li>
                </ul>
                <p>Нашей компанией были спроектрированы и осуществлены несколько десятков проектов.</p>
                <p>Самыми последними стали объекты в Сочи к Олимпиаде-2014:</p>
                <ul>
                    <li><p>Инженерная защита территории, кабельных и воздушных линий (110кВ) в районе плато Роза Хутор (селезащитная галерея) в Краснодарском крае, Красная поляна.</p></li>
                    <li><p>Проект совмещенной автомобильной и железной дороги «Адлер - Альпика Сервис».</p></li>
                    <li><p>Производство и поставка олимпийской символики «Сочи 2014» в виде больших олимпийских колец, расположенных на крупных магистралях Сочи.</p></li>
                </ul>
                <p>С остальными объектами вы можете ознакомиться в разделе сайта: «Галерея Объектов» и в разделе «Новости».</p>
                <p><strong>Приглашаем вас к взаимовыгодному сотрудничеству!</strong></p>
            </article>
    <!--</div>-->
  </section>
<!--background div end-->
</div>





<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>